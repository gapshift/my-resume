// Modular components
import Section from './Section';
import SectionTitle from './SectionTitle';
import Skill from './Skill';
import Hero from './sections/Hero';
import TimelineEntry from './TimelineEntry';
import VerticalSpacer from './VerticalSpacer';
import EducationCard from './EducationCard';
import ReferenceBlock from './ReferenceBlock';
import CustomButton from './CustomButton';
import Phonetics from './Phonetics';
import NavBar from './NavBar';

// Section components
import AboutMe from './sections/AboutMe';
import Skills from './sections/Skills';
import Experience from './sections/Experience';
import Education from './sections/Education';
import References from './sections/References';
import CallToAction from './sections/CallToAction';
import Contact from './sections/Contact';

export {
    Section,
    Hero,
    SectionTitle,
    AboutMe,
    Skill,
    Skills,
    Experience,
    Education,
    References,
    Contact,
    TimelineEntry,
    VerticalSpacer,
    CallToAction,
    EducationCard,
    ReferenceBlock,
    CustomButton,
    Phonetics,
    NavBar,
};
