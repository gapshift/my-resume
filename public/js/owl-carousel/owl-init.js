var initOwl;

jQuery.noConflict();
(function($) {
    // init
    var animSpeed = 500;
    var ease = 'cubic-bezier(.06,.64,.23,1.15)';
    var transient = {}; // Empty object where we can store current item's index before drag
    var events;
    var onDrag;
    var onDragged;

    onDrag = function(event) {
        this.initialCurrent = event.relatedTarget.current();
        this.owlId = event.target;
        // console.log(event.page.index);
    };

    onDragged = function(event) {
        var owl = event.relatedTarget;
        var draggedCurrent = owl.current();

        if (draggedCurrent > this.initialCurrent) {
            owl.current(this.initialCurrent);
            owl.next();
        } else if (draggedCurrent < this.initialCurrent) {
            owl.current(this.initialCurrent);
            owl.prev();
        }
    };

    events = {
        onDrag: onDrag.bind(transient),
        onDragged: onDragged.bind(transient),
    };

    // Education slider
    var edu_car_options = {
        slideTransition: ease,
        navSpeed: animSpeed,
        dotsSpeed: animSpeed,
        dragEndSpeed: animSpeed,
        dots: true,
        nav: false,
        stagePadding: 40,
        margin: 20,
        responsive: {
            0: {
                items: 1,
            },
            768: {
                items: 2,
            },
            992: {
                items: 3,
            },
        },
    };
    initOwl = function() {
        $('#edu-slider').owlCarousel(Object.assign(edu_car_options, events));
    };
    // initOwl();
    $('#edu-slider').owlCarousel(Object.assign(edu_car_options, events));
})(jQuery);
